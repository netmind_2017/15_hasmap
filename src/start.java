import java.util.HashMap;
import java.util.Map.Entry;
		
public class start {

	public static void main(String[] args) {
		
		/*
		HashMap<Integer,String> items =new HashMap<Integer,String>();  
		items.put(1,"Andrea");  
		items.put(2,"Paolo");
		items.put(3,"Grabiele");
		items.put(4,"John"); 
		
		System.out.println("size:" +  items.size());
		
		for(Entry<Integer, String> item: items.entrySet() ){  
		 System.out.println(item.getKey()+ " " +item.getValue());  
		} 
		*/
		
		
		HashMap<Integer,Person> listPerson =new HashMap<Integer,Person>();
		Person x = new Person("andrea","gelsomino",40);
		listPerson.put(1,x);
		listPerson.put(2,new Person("Lenny","Kravitz",40));
		listPerson.put(3,new Person("Michael","Jackson",50));
		listPerson.put(4,new Person("Robbie","Williams",43));
		
		for(Entry<Integer, Person> person: listPerson.entrySet() ){  
			 System.out.println(person.getKey()+ " " + person.getValue().name);  
		} 
	
	}
}
